# ibmTest.py
# 
# This file tests all 11 classifiers using the NLClassifier IBM Service
# previously created using ibmTrain.py
# 
# TODO: You must fill out all of the functions in this file following 
# 		the specifications exactly. DO NOT modify the headers of any
#		functions. Doing so will cause your program to fail the autotester.
#
#		You may use whatever libraries you like (as long as they are available
#		on CDF). You may find json, request, or pycurl helpful.
#		You may also find it helpful to reuse some of your functions from ibmTrain.py.
#
import pycurl
import re
import subprocess
from ibmTrain import convert_training_csv_to_watson_csv_format
import urllib2
import sys

def get_classifier_ids(username,password):
	# Retrieves a list of classifier ids from a NLClassifier service 
	# an outputfile named ibmTrain#.csv (where # is n_lines_to_extract).
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#		
	# Returns:
	#	a list of classifier ids as strings
	#
	# Error Handling:
	#	This function should throw an exception if the classifiers call fails for any reason
	#
	
	#TODO: Fill in this function
    classifier_id = []
    try:
        f = open('./classifier_result.csv', 'r').readlines()
    except IOError:
        print "Error"
        return

    try:
        for line in f:
            # dict[classifier['name']] = classifier['classifier_id']      # dict{'classifier 500': id}
            mo = re.search("classifier_id", line)
            if mo:
                classifier_id.append(line[mo.end() + 5:-3])
                # print classifier_id
        return classifier_id
    except:
        print "Error"


	

def assert_all_classifiers_are_available(username, password, classifier_id_list):
	# Asserts all classifiers in the classifier_id_list are 'Available' 
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#	classifier_id_list - a list of classifier ids as strings
	#		
	# Returns:
	#	None
	#
	# Error Handling:
	#	This function should throw an exception if the classifiers call fails for any reason AND 
	#	It should throw an error if any classifier is NOT 'Available'
	#
	
	#TODO: Fill in this function


    for classid in classifier_id_list:
        try:
            output = subprocess.check_output(['sh','./status.sh', username, password, classid])
            mo = re.search("\"status\" : \"Available\"", output)
            if not mo:
                print "Classifier not available"
            else:
                print "Classifier Available.\n"
        except IOError:
            print "classifier call fails"
    return

def classify_single_text(username,password,classifier_id,text):
	# Classifies a given text using a single classifier from an NLClassifier 
	# service
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#	classifier_id - a classifier id, as a string
	#		
	#	text - a string of text to be classified, not UTF-8 encoded
	#		ex. "Oh, look a tweet!"
	#
	# Returns:
	#	A "classification". Aka: 
	#	a dictionary containing the top_class and the confidences of all the possible classes 
	#	Format example:
	#		{'top_class': 'class_name',
	#		 'classes': [
	#					  {'class_name': 'myclass', 'confidence': 0.999} ,
	#					  {'class_name': 'myclass2', 'confidence': 0.001}
	#					]
	#		}
	#
	# Error Handling:
	#	This function should throw an exception if the classify call fails for any reason 
	#
	
	#TODO: Fill in this function

#     remove classifier id url

    # change punctuation into utf-8

    text = urllib2.quote(text, '')
    try:
        result = subprocess.check_output(['sh', './test_classifier.sh', username, password, classifier_id, text])
        # dict = convert2dict(result)
        print result
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    except ValueError:
        print "Could not run sh."
    except:
        print "Fail to call classifier:", sys.exc_info()[0]
        raise
    return result

def convert2dict(result):
    dict = {}
    classes = []
    result = result.split('\n')
    print result
    for i in xrange(len(result) - 1):
        mo_topclass = re.search("top_class", result[i])
        if mo_topclass:
            dict["top_class"] = result[i][mo_topclass.end() + 5:-2]
            print dict["top_class"]
        mo_classname = re.search("class_name", result[i])
        mo_confidence = re.search("confidence \: ", result[i + 1])
        if mo_classname and mo_confidence:
            dict2 = {}
            dict2["class name"] = result[i][mo_classname.end() + 5:-3]
            dict2["confidence"] = result[i][mo_confidence.end() + 5:]
            classes.append(dict2)
    dict["classes"] = classes
    return dict

def classify_all_texts(username,password,input_csv_name):
	# Classifies all texts in an input csv file using all classifiers for a given NLClassifier 
	# service. 
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#	
	#	input_csv_name - full path and name of an input csv file in the  
	#		6 column format of the input test/training files
	#
	# Returns:
	#	A list of "classifications". Aka: 
	#	A list of dictionaries, one for each text, in order of lines in the 
	#	input file. Each element is a dictionary containing the top_class
	#	and the confidences of all the possible classes (ie the same
	#	format as returned by classify_single_text) 
	#	Format example:
	#		[
	#			{'top_class': 'class_name',
	#		 	 'classes': [
	#					  	{'class_name': 'myclass', 'confidence': 0.999} ,
	#					  	{'class_name': 'myclass2', 'confidence': 0.001}
	#						]
	#			},
	#			{'top_class': 'class_name',
	#			...
	#			}
	#		]
	#
	# Error Handling:
	#	This function should throw an exception if the classify call fails for any reason 
	#	or if the input csv file is of an improper format.
	#
	
	#TODO: Fill in this function
	
	return
		

def compute_accuracy_of_single_classifier(classifier_dict, input_csv_file_name):
	# Given a list of "classifications" for a given classifier, compute the accuracy of this
	# classifier according to the input csv file
	#
	# Inputs:
	# 	classifier_dict - A list of "classifications". Aka:
	#		A list of dictionaries, one for each text, in order of lines in the 
	#		input file. Each element is a dictionary containing the top_class
	#		and the confidences of all the possible classes (ie the same
	#		format as returned by classify_single_text) 	
	# 		Format example:
	#			[
	#				{'top_class': 'class_name',
	#			 	 'classes': [
	#						  	{'class_name': 'myclass', 'confidence': 0.999} ,
	#						  	{'class_name': 'myclass2', 'confidence': 0.001}
	#							]
	#				},
	#				{'top_class': 'class_name',
	#				...
	#				}
	#			]
	#
	#	input_csv_name - full path and name of an input csv file in the  
	#		6 column format of the input test/training files
	#
	# Returns:
	#	The accuracy of the classifier, as a fraction between [0.0-1.0] (ie percentage/100). \
	#	See the handout for more info.
	#
	# Error Handling:
	# 	This function should throw an error if there is an issue with the 
	#	inputs.
	#
	
	#TODO: fill in this function
	
	return

def compute_average_confidence_of_single_classifier(classifier_dict, input_csv_file_name):
	# Given a list of "classifications" for a given classifier, compute the average 
	# confidence of this classifier wrt the selected class, according to the input
	# csv file. 
	#
	# Inputs:
	# 	classifier_dict - A list of "classifications". Aka:
	#		A list of dictionaries, one for each text, in order of lines in the 
	#		input file. Each element is a dictionary containing the top_class
	#		and the confidences of all the possible classes (ie the same
	#		format as returned by classify_single_text) 	
	# 		Format example:
	#			[
	#				{'top_class': 'class_name',
	#			 	 'classes': [
	#						  	{'class_name': 'myclass', 'confidence': 0.999} ,
	#						  	{'class_name': 'myclass2', 'confidence': 0.001}
	#							]
	#				},
	#				{'top_class': 'class_name',
	#				...
	#				}
	#			]
	#
	#	input_csv_name - full path and name of an input csv file in the  
	#		6 column format of the input test/training files
	#
	# Returns:
	#	The average confidence of the classifier, as a number between [0.0-1.0]
	#	See the handout for more info.
	#
	# Error Handling:
	# 	This function should throw an error if there is an issue with the 
	#	inputs.
	#
	
	#TODO: fill in this function
	
	return


if __name__ == "__main__":

	# input_test_data = '<ADD FILE NAME HERE>'

    username = "bee79396-cad1-4f49-9e7e-42fde7a1349b"
    password = "0xnQMhbu1shc"
    # classifier_id = get_classifier_ids(username, password)

	#STEP 1: Ensure all 11 classifiers are ready for testing
    # assert_all_classifiers_are_available(username, password, classifier_id)

	#STEP 2: Test the test data on all classifiers

    #STEP 2.1: convert test data to two-field format,  set group_id to 0 so that all the lines are converted (because
    #  for the training case, only a subset is converted according to group_id)
    # test_csv = "testdata.manualSUBSET.2009.06.14.csv"
    # test_transform = "ibmTestSUBSET.csv"
    # convert_training_csv_to_watson_csv_format(test_csv, test_transform, 0)

    # f = open('./tweets/' + test_transform, 'r').readlines()
    # text = f[0][:-3]
    # print text
    # classify_single_text(username, password, "c7fa4ax22-nlc-2", text)

	#STEP 3: Compute the accuracy for each classifier
	
	#STEP 4: Compute the confidence of each class for each classifier
	

    text = open('./tweets/ibmTestSUBSET.csv', 'r').readlines()[0][:-3]
    text = urllib2.quote(text, '')
    print text
    classify_single_text(username, password, "c7fa4ax22-nlc-2", text)