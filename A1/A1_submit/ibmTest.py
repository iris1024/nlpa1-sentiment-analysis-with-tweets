# ibmTest.py
# 
# This file tests all 11 classifiers using the NLClassifier IBM Service
# previously created using ibmTrain.py
# 
# TODO: You must fill out all of the functions in this file following 
# 		the specifications exactly. DO NOT modify the headers of any
#		functions. Doing so will cause your program to fail the autotester.
#
#		You may use whatever libraries you like (as long as they are available
#		on CDF). You may find json, request, or pycurl helpful.
#		You may also find it helpful to reuse some of your functions from ibmTrain.py.
#
from ibmTrain import convert_training_csv_to_watson_csv_format
import urllib2
import sys
import requests
from requests.auth import HTTPBasicAuth

def get_classifier_ids(username,password):
	# Retrieves a list of classifier ids from a NLClassifier service 
	# an outputfile named ibmTrain#.csv (where # is n_lines_to_extract).
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#		
	# Returns:
	#	a list of classifier ids as strings
	#
	# Error Handling:
	#	This function should throw an exception if the classifiers call fails for any reason
	#
	
	#TODO: Fill in this function
    classifier_id = []
    try:
        r = requests.get("https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers",
                     auth = HTTPBasicAuth(username, password))
        class_info_dict = r.json()
        for classifier in class_info_dict['classifiers']:
            classifier_id.append(classifier['classifier_id'])
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    except ValueError:
        print "Incorrect username and password"
    except:
        print "Fail to call classifier:", sys.exc_info()[0]
        raise

    return classifier_id


def assert_all_classifiers_are_available(username, password, classifier_id_list):
	# Asserts all classifiers in the classifier_id_list are 'Available' 
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#	classifier_id_list - a list of classifier ids as strings
	#		
	# Returns:
	#	None
	#
	# Error Handling:
	#	This function should throw an exception if the classifiers call fails for any reason AND 
	#	It should throw an error if any classifier is NOT 'Available'
	#
	
	#TODO: Fill in this function
    try:
        for classid in classifier_id_list:
            r = requests.get("https://gateway.watsonplatform.net/natural-language-classifier/api/v1/classifiers/" +
                          classid, auth = HTTPBasicAuth(username, password))
            status_dict = r.json()
            if status_dict['status'] == 'Available':
                print "Classifier Available.\n"
            else:
                print
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    except ValueError:
        print "Incorrect username and password."
    except:
        print "Fail to call classifier:", sys.exc_info()[0]
        raise
    return

def classify_single_text(username,password,classifier_id,text):
	# Classifies a given text using a single classifier from an NLClassifier 
	# service
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#
	#	classifier_id - a classifier id, as a string
	#		
	#	text - a string of text to be classified, not UTF-8 encoded
	#		ex. "Oh, look a tweet!"
	#
	# Returns:
	#	A "classification". Aka: 
	#	a dictionary containing the top_class and the confidences of all the possible classes 
	#	Format example:
	#		{'top_class': 'class_name',
	#		 'classes': [
	#					  {'class_name': 'myclass', 'confidence': 0.999} ,
	#					  {'class_name': 'myclass2', 'confidence': 0.001}
	#					]
	#		}
	#
	# Error Handling:
	#	This function should throw an exception if the classify call fails for any reason 
	#
	
	#TODO: Fill in this function


    # convert punctuation into utf-8

    text = urllib2.quote(text, '')

    test_result_dict = {}
    try:
        r = requests.get("https://gateway.watsonplatform.net/natural-language-classifier/api/v1"
                                      "/classifiers/" +
                                 classifier_id + "/classify?text=" + text, auth = HTTPBasicAuth(username, password))
        test_result_dict = r.json()
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    except ValueError:
        print "Incorrect username and password."
    except:
        print "Fail to call classifier:", sys.exc_info()[0]
        raise

    return test_result_dict


def classify_all_texts(username,password,input_csv_name):
	# Classifies all texts in an input csv file using all classifiers for a given NLClassifier 
	# service. 
	#
	# Inputs: 
	# 	username - username for the NLClassifier to be used, as a string
	#
	# 	password - password for the NLClassifier to be used, as a string
	#	
	#	input_csv_name - full path and name of an input csv file in the  
	#		6 column format of the input test/training files
	#
	# Returns:
	#	A list of "classifications". Aka: 
	#	A list of dictionaries, one for each text, in order of lines in the 
	#	input file. Each element is a dictionary containing the top_class
	#	and the confidences of all the possible classes (ie the same
	#	format as returned by classify_single_text) 
	#	Format example:
	#		[
	#			{'top_class': 'class_name',
	#		 	 'classes': [
	#					  	{'class_name': 'myclass', 'confidence': 0.999} ,
	#					  	{'class_name': 'myclass2', 'confidence': 0.001}
	#						]
	#			},
	#			{'top_class': 'class_name',
	#			...
	#			}
	#		]
	#
	# Error Handling:
	#	This function should throw an exception if the classify call fails for any reason 
	#	or if the input csv file is of an improper format.
	#
	
	#TODO: Fill in this function
    try:
        # input_csv_name: converted ibm formatted test file under my root
        file = open('./' + input_csv_name, 'r').readlines()
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        return

    result = []
    output = open('./4output.txt', 'w') # 4output.txt under my root
    classifier_id = get_classifier_ids(username, password)

    print "Classification in progress..."
    try:
        n_sample = [5000, 2500, 500]
        for (classifier, n)in zip(classifier_id, n_sample):
            output.write("classifier " + str(n) + ": " + classifier + "\n")
            dict_list = []
            for text in file:
                classifier_dict = classify_single_text(username, password, classifier, text)
                dict_list.append(classifier_dict)
            result.extend(dict_list)
            print "Classification done.\n"
            print "computing accuracy"
            accuracy = compute_accuracy_of_single_classifier(dict_list, input_csv_name)
            print "computing confidence"
            confidence_dict = compute_average_confidence_of_single_classifier(dict_list, input_csv_name)
            output.write("accuracy: " + str(accuracy) + "\n")
            output.write("confidence: " + str(confidence_dict) + "\n\n")
        output.write("\n\n\n")
        output.close()
    except:
        print "Fail to call classifier:", sys.exc_info()[0]

    print "Classification done."

    return result
		

def compute_accuracy_of_single_classifier(classifier_dict, input_csv_file_name):
	# Given a list of "classifications" for a given classifier, compute the accuracy of this
	# classifier according to the input csv file
	#
	# Inputs:
	# 	classifier_dict - A list of "classifications". Aka:
	#		A list of dictionaries, one for each text, in order of lines in the 
	#		input file. Each element is a dictionary containing the top_class
	#		and the confidences of all the possible classes (ie the same
	#		format as returned by classify_single_text) 	
	# 		Format example:
	#			[
	#				{'top_class': 'class_name',
	#			 	 'classes': [
	#						  	{'class_name': 'myclass', 'confidence': 0.999} ,
	#						  	{'class_name': 'myclass2', 'confidence': 0.001}
	#							]
	#				},
	#				{'top_class': 'class_name',
	#				...
	#				}
	#			]
	#
	#	input_csv_name - full path and name of an input csv file in the  
	#		6 column format of the input test/training files
	#
	# Returns:
	#	The accuracy of the classifier, as a fraction between [0.0-1.0] (ie percentage/100). \
	#	See the handout for more info.
	#
	# Error Handling:
	# 	This function should throw an error if there is an issue with the 
	#	inputs.
	#
	
	#TODO: fill in this function

    try:
        file = open('./' + input_csv_file_name, 'r').readlines() # converted ibm formatted test data under my root
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        return

    if len(file) == 0:
        return 0

    count = 0
    total = len(file)

    for (dict, line) in zip(classifier_dict, file):
        tag = line[-2]
        if tag == '2':
            total -= 1
            continue
        test_class = dict['top_class']
        count += tag == test_class
    if total != 0:
        return float(count) / total

	return 0

def compute_average_confidence_of_single_classifier(classifier_dict, input_csv_file_name):
	# Given a list of "classifications" for a given classifier, compute the average 
	# confidence of this classifier wrt the selected class, according to the input
	# csv file. 
	#
	# Inputs:
	# 	classifier_dict - A list of "classifications". Aka:
	#		A list of dictionaries, one for each text, in order of lines in the 
	#		input file. Each element is a dictionary containing the top_class
	#		and the confidences of all the possible classes (ie the same
	#		format as returned by classify_single_text) 	
	# 		Format example:
	#			[
	#				{'top_class': 'class_name',
	#			 	 'classes': [
	#						  	{'class_name': 'myclass', 'confidence': 0.999} ,
	#						  	{'class_name': 'myclass2', 'confidence': 0.001}
	#							]
	#				},
	#				{'top_class': 'class_name',
	#				...
	#				}
	#			]
	#
	#	input_csv_name - full path and name of an input csv file in the  
	#		6 column format of the input test/training files
	#
	# Returns:
	#	The average confidence of the classifier, as a number between [0.0-1.0]
	#	See the handout for more info.
	#
	# Error Handling:
	# 	This function should throw an error if there is an issue with the 
	#	inputs.
	#
	
	#TODO: fill in this function
    try:
        f = open('./' + input_csv_file_name, 'r').readlines()  # converted ibm formatted test data under my root
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

    confidence_dict = {}
    confidence_dict['correct'] = 0
    confidence_dict['incorrect'] = 0
    num_correct = 0
    num_incorrect = 0

    for (dict,  line)in zip(classifier_dict, f):
        tag = line[-2]
        test_class = dict['top_class']
        for classes in dict['classes']:
            if classes['class_name'] == test_class:
                confidence = classes['confidence']
        if test_class == tag:
            confidence_dict['correct'] += confidence
            num_correct += 1
        else:
            confidence_dict['incorrect'] += confidence
            num_incorrect += 1
    confidence_dict['correct'] /= num_correct
    confidence_dict['incorrect'] /= num_incorrect

    return confidence_dict


username = "bee79396-cad1-4f49-9e7e-42fde7a1349b"
password = "0xnQMhbu1shc"

if __name__ == "__main__":

    input_test_data = 'testdata.manualSUBSET.2009.06.14.csv' # under /u/cs401/A1
    test_convert = "ibmTestSUBSET.csv"    # csv file with ibm watson, format under my root

    #STEP 0: # this function takes groupid to process part of the training data in ibmTrain part,
    # for this part, all the lines of test data are processed,
    # so we set groupid to be 0, and the function will process all the lines of the input
    convert_training_csv_to_watson_csv_format(input_test_data, test_convert, 0)

    classifier_id = get_classifier_ids(username, password)


	#STEP 1: Ensure all 11 classifiers are ready for testing
    assert_all_classifiers_are_available(username, password, classifier_id)

	#STEP 2: Test the test data on all classifiers

	#STEP 3: Compute the accuracy for each classifier

	#STEP 4: Compute the confidence of each class for each classifier

    # STEP 2-4 are implemented by the following
    classify_all_texts(username, password, test_convert)   # test_convert is under my root
