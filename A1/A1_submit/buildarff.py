__author__ = 'weiweili'
import re
import sys
import os

firstperson = open('./First-person', 'r').read().replace('\n', '/|')+ '/'
secondperson = open('./Second-person', 'r').read().replace('\n', '/|')+ '/'
thirdperson = open('./Third-person', 'r').read().replace('\n', '/|') + '/'
slang = open('./Slang', 'r').read().replace('\n', '/|') + '/'

# add the following lines for bonus
# afinn = open('./AFINN-111.txt').readlines()
#
# word_score_dict = {}
# for line in afinn:
#     match = re.search('(?P<word>([a-zA-Z]+\s+)+)(?P<word_score>-?\d)', line)
#     if match:
#         word = match.group('word').strip()
#         word_score = match.group('word_score')
#         word_score_dict[word] = word_score

# end of bonus

# Bonus: added feature
positive_emoticon = open('./Positive_Emoticon.txt', 'r').read().replace('\n', '/|')+ '/'
negative_emoticon = open('./Negative_Emoticon.txt', 'r').read().replace('\n', '/|')+ '/'
#####

features = [['First_person_pronouns', '^(' + firstperson + ')'],
          ['Second_person_pronouns',  '^(' + secondperson + ')'],
          ['Third_person_pronouns', '^(' + thirdperson + ')'],
          ['Coordinating_conjunctions', '/CC$'],  # use word list provided
          ['Past-tense_verbs', '/VBD$'],
          ['Future-tense_verbs', '(\'ll/MD|will/MD|gonna/VBG|going/VBG to/TO)'],
          ['Commas', ',/,'],
          ['Colons_and_semi-colons', '\:|\;'],
          ['Dashes', '-/|\w+?-\w+?/'],
          ['Parentheses', '\(|\)'],
          ['Ellipses', '\.{2,}/'],
          ['Common_nouns', '(/NN|/NNS)$'],
          ['Proper_nouns', '(/NNP|/NNPS)$'],
          ['Adverbs', '(/RB|/RBR|/RBS)$'],
          ['wh-words', '(\/WDT|\/WP|\/WP\$|\/WRB)$'],
          ['Slang', '^(' + slang + ')'],
          ['Words_all_in_upper_case', '^[A-Z]{2,}/'],
          # ['Positive_Emoticons', '^(' + positive_emoticon + ')NN$'],   # added for bonus
          # ['Negative_Emoticons', '^(' + negative_emoticon + ')NN$'],   # added for bonus
          # ['Sentiment_score'], # add for bonus
          ['Average_length_of_sentences'],
          ['Average_length_of_tokens'],
          ['Number_of_sentences']
        ]

# calculate the counts for each feature
def count(twt, arff):
    sentences = twt.split('\n')
    sentences.remove('')
    twt = twt.split( )

    for feature in features:
        count = 0
        if feature[0] == 'Future-tense_verbs':
            for sentence in sentences:
                count += len(re.findall(feature[1], sentence, re.IGNORECASE))
                count -= len(re.findall('(\'ll/MD|will/MD) be/VB going/VBG to/TO', sentence, re.IGNORECASE))
        elif len(feature) == 2 and feature[0] != 'Words_all_in_upper_case':
            for word in twt:
                if re.search(feature[1], word, re.IGNORECASE):
                    count += 1
        elif feature[0] == 'Words_all_in_upper_case':
            for word in twt:
                if re.search(feature[1], word):
                    count += 1
        else:
            if feature[0] == 'Average_length_of_sentences':
                if len(sentences) != 0:
                    count = float(len(twt)) / len(sentences)
                else:
                    count = 0
            if feature[0] == 'Average_length_of_tokens':
                count = len_tokens(twt)
            if feature[0] == 'Number_of_sentences':
                count = len(sentences)

            # add the following lines for bonus
            # if feature[0] == 'Sentiment_score':
            #     for word in twt:
            #         match = re.search(r'(?P<to_compare_word>\w+)/', word, flags = re.IGNORECASE)
            #         if match:
            #             word = match.group('to_compare_word').lower()
            #         if word in word_score_dict:
            #          count += int(word_score_dict[word])
            # end of bonus
        arff.write(str(count) + ', ')



# average length (in characters) of tokens excluding punctuation tokens and class number
def len_tokens(twt):
    num_char = 0
    num_word = len(twt)
    punc = '^\W' # any non-word
    for word in twt:
        if re.search(punc, word) and not re.search('^(' + positive_emoticon + ')NN$', word) \
                and not re.search('^(' + negative_emoticon + ')NN$', word):   # to revise
            num_word -= 1
            continue
        word = word.split('/\w')
        num_char += len(word[0])
    if num_word == 0:
        return 0
    return float(num_char) / num_word   # int or double?


def buildarff(infile, outfile, maxnum = -1):
    classtags = re.findall('<A=(\d+)>\n', infile)
    infile = re.split('<A=\d+>\n', infile)
    infile.remove('')


    if maxnum != -1 and maxnum * 2 < len(classtags):
        infile1 = infile[:maxnum]
        infile2 = infile[len(classtags) / 2:len(classtags) / 2 + maxnum]
        infile1.extend(infile2)
        infile = infile1
        classtags1 = classtags[:maxnum]
        classtags2 = classtags[len(classtags) / 2:len(classtags) / 2 + maxnum]
        classtags1.extend(classtags2)
        classtags = classtags1
    index = 0
    for tweet in infile:
        count(tweet, outfile)
        outfile.write(classtags[index]+'\n')
        index += 1

def write_arff(twtpath, arffpath, maxnum=-1):
    input = open(twtpath, 'r').read()
    output = open(arffpath, 'w')
    mo = re.search(r"(\w+)", twtpath)  # extract the word train or test from the path
    output.write('@relation ' + mo.group(0) +'\n\n')
    for feature in features:
        output.write('@attribute ' + feature[0] + ' numeric\n')
    output.write('@attribute class {0, 4}')
    # add classification label
    output.write('\n@data\n')

    buildarff(input, output, maxnum)
    output.close()

argv_len = len(sys.argv)

if argv_len < 2 :
	print "Please enter input filename, output filename, and max number (optional)."
	sys.exit()

else:
	print "Building arff file"
	if (os.path.exists(sys.argv[1]) == False):
		print "File not exist."
		sys.exit()
	input_path = sys.argv[1]
	output_path = './' + sys.argv[2]

maxnum = -1
if argv_len == 4:
    maxnum = int(sys.argv[3])

write_arff(input_path, output_path, maxnum)
print "Building done."

