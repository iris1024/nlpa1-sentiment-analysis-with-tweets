#Pre-processing, tokenizing, and tagging
import os
import sys
sys.path.append('/u/cs401/Wordlists/')
sys.path.append('/u/cs401/A1/tagger/')
import re
import NLPlib 

def field_filter(line):
    linelist = line.split('","')
    line = linelist[0] + '"' + linelist[5]
    return line

def html_tags(line):
	line = re.sub(r'<[^>]+>', '', line)
	return line

html_codes = {
	'&quot;':'"',
	'&amp;':'&',
	'&lt;':'<',
	'&gt;':'>',
}

def html_code(line):
	while (re.findall(r'&\w+;',line)):
		for key in html_codes:
			line = re.sub(key, html_codes[key], line)
	if re.search(r'&\w+;',line):
		print "Caution: find new html code!!"
		print re.search(r'&\w+;',line)
	return line

def urls(line):
	line = re.sub(r'(http|https|ftp):\/\/\S+( |$)','', line, flags=re.IGNORECASE)
	line = re.sub(r'www\.\w+\S*( |$)', '', line, flags=re.IGNORECASE)
	return line

def user_tags(line):
	while (re.findall(r'(@|#)(?P<reserve>\w+\S*( |$))',line)):
          reserve = re.search(r'(@|#)(?P<reserve>\w+\S*( |$))',line)
          reserve = reserve.group('reserve')
          line = re.sub(r'(@|#)(?P<reserve>\w+\S*( |$))', reserve, line, 1)
	length = len(line)
	if line[length - 2] == '"':
		line = line[:length - 2] + line[length-1]
	return line

def abbrev():
	path = '/u/cs401/Wordlists/abbrev.english'
	abbr = open(path, 'r')
	abbr_lines = abbr.readlines()
	abbr_dict = []
	for abbrev in abbr_lines:
		abbrev = abbrev.rstrip('\n')
		abbr_dict.append(abbrev)
	return abbr_dict

def end_of_sen(line):
	match = re.finditer(r'(?P<word_before>(\w+)|\s)(?P<punc>(\.|\?|\!))(?P<next_puncs>(\.|\?|\!|\s)*)(?P<next_char>(.|$)?)', line)
	linelist = []
	to_split_index = []
	for each in match:
		start_index = each.start()
		end_index = each.end()
		to_compare = each.group('word_before') + each.group('punc')
		punc = each.group('punc')
		next_puncs = each.group('next_puncs')
		next_char = each.group('next_char')
		if punc == '.':
			if to_compare in abbr_dict:
				if ( re.search(r'\s+', next_puncs) and next_char.isupper()):
					to_split_index.append(end_index - 1)
			else :
				#P.Y.T, 3.0
				to_check = to_compare + next_puncs + next_char
				if (re.search(r'(\d\.\d)|([A-Z]\.[A-Z])', to_check)):
					#print to_check
					to_check = ''
				elif (next_char == ':' or next_char == ';'):
					if (len(line) != end_index) and (line[end_index] in ['D','d','P','p','O','o','@','L','S','s','C','c','\\','/']):
						check_emoticon = line[end_index + 1:]
						if (re.search(r'\s*\w+\s+\w+', check_emoticon)):
							to_split_index.append(end_index - 1)
							#print check_emoticon
				elif (next_char != ':' and next_char != ';' and next_char.isalnum() == True):
					to_split_index.append(end_index - 1)
		else:
			if (next_char != ':' and next_char != ';' and next_char.isalnum() == True):
				to_split_index.append(end_index - 1)

	length = len(line)
	to_split_index.append(length-1)
	last_index = 0
	for index in to_split_index:
		# print line[last_index:index]
		linelist.append(line[last_index:index])
		last_index = index
	return linelist

def add_space(linelist):
	lineindex = 0
	for line in linelist:
		match = re.finditer(r'(?P<replace>[\.\?!,:;]+)', line)
		count = 0
		for each in match:
			replace = each.group('replace')
			start = each.start()
			end = each.end()
			to_check = ''
			if (len(line) != end):
				to_check = line[start + count - 1] + replace + line[end]
				if (re.search(r'(\d\.\d)|([A-Z]\.[A-Z])|(\d:\d)', to_check)):
					#print to_check
					to_check =  ''
			#remove :D :d :P :p :O :o :@ :L :S :s :C :c :/ :\
			#:-p :-P :-D :-O :-o :-* :-/ :-\ :-s :-S :-| :-x :-? :-W :-X 
				elif replace in [',', ':', ';']:
					if (len(line) != end) and (line[end] in ['D','d','P','p','O','o','@','L','S','s','C','c','\\','/']) and (len(line) == end + 1 or line[end + 1] == ' '):
						line = line
					#continue
					elif (len(line) != end) and (line[end] == '-') and (len(line) >= end + 2):
						line = line
					#continue
					elif (len(line) != end and line[end].isalnum() == True):
						line = line[0:start + count] + ' '  + line[start + count:end + count] + ' ' + line[end + count:]
						count = count + 2
					else :
						line = line[0:start + count] + ' '  + line[start + count:]
						count = count + 1 
				else :
					line = line[0:start + count] + ' '  + line[start + count:end + count] + ' ' + line[end + count:]
					count = count + 2
			elif replace in [',', ':', ';']:
				if (len(line) != end) and (line[end] in ['D','d','P','p','O','o','@','L','S','s','C','c','\\','/']) and (len(line) == end + 1 or line[end + 1] == ' '):
					line = line
				elif (len(line) != end) and (line[end] == '-') and (len(line) >= end + 2): 
					line = line
				elif (len(line) != end and line[end].isalnum() == True):
					line = line[0:start + count] + ' '  + line[start + count:end + count] + ' ' + line[end + count:]
					count = count + 2
				else :
					line = line[0:start + count] + ' '  + line[start + count:]
					count = count + 1 
			else :
				line = line[0:start + count] + ' '  + line[start + count:end + count] + ' ' + line[end + count:]
				count = count + 2
		linelist[lineindex] = line
		lineindex = lineindex + 1
	return linelist


def demarcation(linelist):
	match = re.search(r'\"(?P<myclass>\d)\"', linelist[0])
	myclass = match.group('myclass')
	replace = '<A=' + str(myclass) + '>'
	linelist[0] = re.sub(r'\"(?P<myclass>\d)\"', '', linelist[0])
	linelist.insert(0,replace)
	return linelist


def tokenize(linelist):
	lineindex = 0 
	if (len(linelist) == 2) and re.search(r'^\s*$', linelist[1]) > 0:
		print 'blank tweet'
		linelist[1] = 'blanklineafterpreprocessing'
		return linelist
	for line in linelist:
		# ""only"" ->  "" only "" for test dataset
		match =  re.finditer(r'\"\"', line)
		for each in match:
			line = re.sub(r'\"\"', ' \"\" ', line)
		# TODO how to deal with I/0, %windir%\system32\ 
		# dog's -> dog 's
		match = re.finditer(r'(?P<word_before>\w+)\'(?P<clitic>(s|m|re|d|ll|ve|t))', line, flags=re.IGNORECASE)
		count = 0
		for each in match:
			clitic = each.group('clitic')
			word_before = each.group('word_before')
			start = each.start()
			end = each.end()
			# I'm, you're, she's, he'd / they'll, they've / can't, it's
			if clitic == 't' and line[end+count-3] == 'n': #can't -> ca n't
				line = line[:end+count-3] + ' ' + line[end+count-3:]
				count = count + 1
			elif len(clitic) == 1: #retain it's
				#if word_before not in ['it', 'It', 'IT', 'she', 'She', 'SHE', 'he', 'He', 'HE', 'that', 'That', 'THAT']:
				line = line[:end+count-2] + ' ' + line[end+count-2:]
				count = count + 1
			elif len(clitic) == 2: #I'll -> I 'll
				line = line[:end+count-3] + ' ' + line[end+count-3:]
				count = count + 1
			#line = re.sub(r'(?P<word_before>\w+)\'s', replace, line, 1, flags=re.IGNORECASE)
		# dogs' -> dogs '
		while (re.findall(r's\'', line, flags=re.IGNORECASE)):
			line = re.sub(r's\'', 's \'', line, flags=re.IGNORECASE)
		# 'Star Trek' -> ' Star Trek' 
		match = re.finditer(r'\'(?P<clitic>\w+)',line, flags=re.IGNORECASE)
		count = 0
		for each in match:
			clitic = each.group('clitic')
			start = each.start()
			end = each.end()
			if clitic not in ['m', 'M', 're', 'RE', 'd', 'D', 'll', 'LL', 've', 'VE', 't', 'T', 's', 'S']:
				line = line[:start+count+1] + ' ' + line[start+count+1:] 
				count = count + 1
		# Trek' -> Trek '		
		match = re.finditer(r'(?P<clitic>\w+)\'\s',line, flags=re.IGNORECASE)
		count = 0
		for each in match:
			start = each.start()
			end = each.end()
			line = line[:end+count-2] + ' ' + line[end+count-2:]
			count = count + 1
		#line = re.split(r'\s+',line)		
		linelist[lineindex] = line
		lineindex = lineindex + 1

	return linelist

def pos(linelist):
	tagger = NLPlib.NLPlib()
	line_done = []
	for line in linelist:
		if (re.search(r'<A=\d>', line) > 0):
			line_done.append(line)
			continue
		if  (re.search(r'^\s*$', line) > 0):
			#print 'Now in blank line'
			continue
		line = line.strip()
		sent = re.split(r'\s+', line)
		tags = tagger.tag(sent)
		tags_union = []
		for tagindex in range(len(tags)):
			tags_union.append(sent[tagindex] + '/' + tags[tagindex])
		line_done.append(' '.join(tags_union))
	return line_done

def processing(line):
    line = field_filter(line)
    line = html_tags(line)
    line = html_code(line)
    line = urls(line)
    line = user_tags(line)
    linelist = []
    linelist = end_of_sen(line)
    linelist = add_space(linelist)
    linelist = demarcation(linelist)
    linelist = tokenize(linelist)
    return linelist

argv_len = len(sys.argv)

if argv_len < 2 :
	print "Please enter input filename and output filename."
	sys.exit()

elif argv_len == 3 :
	print "Pre-processing for testing data"
	if (os.path.exists(sys.argv[1]) == False):
		print "File not exist."
		sys.exit()
	test_path_in = sys.argv[1]
	saved_path = './' + sys.argv[2]
	f = open(test_path_in, 'r')
	output = open(saved_path, 'w')
	abbr_dict = []
	abbr_dict = abbrev()
	line_list = []
	lineunion = []
	for line in f.readlines():
		line_list = processing(line)
		for l in line_list:
			lineunion.append(''.join(l))
	processed_line = pos(lineunion)
	#print processed_line
	for item in processed_line:
		output.write(item + '\n')

elif argv_len == 4:
	print "Pre-processing for training data"
	if (os.path.exists(sys.argv[1]) == False):
		print "File not exist."
		sys.exit()
	train_path_in = sys.argv[1]
	gid = int(sys.argv[2])
	saved_path = './' + sys.argv[3]
	f = open(train_path_in, 'r')
	output = open(saved_path, 'w')
	abbr_dict = []
	abbr_dict = abbrev()
	temp = f.readlines()
	negative = temp[gid * 5500 : (gid + 1) * 5500]
	positive =  temp[(800000 + gid * 5500) : (800000 + (gid + 1) * 5500)]
	mylines = negative + positive
	line_list = []
	lineunion = []
	for line in mylines:
		line_list = processing(line)
		for l in line_list:
			lineunion.append(''.join(l))
	processed_line = pos(lineunion)
	for item in processed_line:
		output.write(item + '\n')

f.close()
output.close()
