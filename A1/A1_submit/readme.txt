buildarff.py

The following files are changed or added, and submitted together:
First-person    Second-person    Third-person    Slang    
Positive_Emoticon.txt    Negative_Emoticon.txt  AFINN-111.txt



Bonus:

1. We added three new features: positive emoticon, negative emoticon, and sentiment score. (uncomment the corresponding lines of positive emoticon, negative emoticon, and sentiment score in buildarff.py). We built Positive_Emoticon.txt and Negative_Emoticon.txt with reference to the emoticon list on wikipedia. For the sentiment score feature, we obtained the AFINN database (saved as AFINN-11.txt) of sentiment words from http://www2.imm.dtu.dk/pubdb/views/publication_details.php?id=6010. And then repeated part3 and put results in bonus_3.*output.txt.

2. We have added more classifiers. Results are included in bonus_alternative_classifiers.txt




