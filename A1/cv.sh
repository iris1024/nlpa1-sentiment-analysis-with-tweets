#!/bin/sh
echo '-------------------------------SVM-------------------------------' >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train0.arff -T cv_partition/cv_test0.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train1.arff -T cv_partition/cv_test1.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train2.arff -T cv_partition/cv_test2.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train3.arff -T cv_partition/cv_test3.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train4.arff -T cv_partition/cv_test4.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train5.arff -T cv_partition/cv_test5.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train6.arff -T cv_partition/cv_test6.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train7.arff -T cv_partition/cv_test7.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train8.arff -T cv_partition/cv_test8.arff -v -o -i >> bonus/3.4SVM.txt
java -cp WEKA/weka.jar weka.classifiers.functions.SMO -t cv_partition/cv_train9.arff -T cv_partition/cv_test9.arff -v -o -i >> bonus/3.4SVM.txt
echo '-------------------------------NB-------------------------------' >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train0.arff -T cv_partition/cv_test0.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train1.arff -T cv_partition/cv_test1.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train2.arff -T cv_partition/cv_test2.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train3.arff -T cv_partition/cv_test3.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train4.arff -T cv_partition/cv_test4.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train5.arff -T cv_partition/cv_test5.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train6.arff -T cv_partition/cv_test6.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train7.arff -T cv_partition/cv_test7.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train8.arff -T cv_partition/cv_test8.arff -v -o -i >> bonus/3.4NB.txt
java -cp WEKA/weka.jar weka.classifiers.bayes.NaiveBayes -t cv_partition/cv_train9.arff -T cv_partition/cv_test9.arff -v -o -i >> bonus/3.4NB.txt
echo '-------------------------------Trees-------------------------------' >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train0.arff -T cv_partition/cv_test0.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train1.arff -T cv_partition/cv_test1.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train2.arff -T cv_partition/cv_test2.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train3.arff -T cv_partition/cv_test3.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train4.arff -T cv_partition/cv_test4.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train5.arff -T cv_partition/cv_test5.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train6.arff -T cv_partition/cv_test6.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train7.arff -T cv_partition/cv_test7.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train8.arff -T cv_partition/cv_test8.arff -v -o -i >> bonus/3.4Trees.txt
java -cp WEKA/weka.jar weka.classifiers.trees.J48 -t cv_partition/cv_train9.arff -T cv_partition/cv_test9.arff -v -o -i >> bonus/3.4Trees.txt
