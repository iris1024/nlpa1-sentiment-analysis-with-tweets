import sys

input_path = sys.argv[1]
output_path = sys.argv[2]
#acu_path = sys.argv[3]
input_file = open(input_path, 'r')
output_file = open(output_path, 'a')
#acu_file = open(acu_output, 'w')

lines = input_file.readlines()

acu_index = 5
class0_index = 18
class4_index = 19
step = 28

			   #Correctly Classified Instances         704               63.9419 %
               # TP Rate   FP Rate   Precision   Recall  F-Measure   ROC Area  Class
               #   0.617     0.338      0.646     0.617     0.631      0.639    0
               #   0.662     0.383      0.633     0.662     0.647      0.639    4
output_file.write('di    	Accuracy    Precision(Class 0)    Recall(Class 0)    Precision(Class 4)    Recall(Class 4)\n')


acu_list = []
for i in range(10):
	acu = lines[acu_index][57:66]
	acu_list.append(acu)
	class0_pre = lines[class0_index][38:43]
	class0_recall = lines[class0_index][48:53]
	class4_pre = lines[class4_index][38:43]
	class4_recall = lines[class4_index][48:53]
	acu_index = acu_index + step
	class0_index = class0_index + step
	class4_index = class4_index + step
	output = str(i) + '      ' + str(acu) + '      ' + str(class0_pre) + '                  ' + str(class0_recall) + '                  ' + str(class4_pre) + '                 ' + str(class4_recall) + '\n' 
	output_file.write(output)
	#print acu, class0_pre, class0_recall, class4_pre, class4_recall
print '---------acu_list------------'
print acu_list

input_file.close()
output_file.close()

