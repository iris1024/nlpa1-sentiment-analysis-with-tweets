# ibmTrain.py
# 
# This file produces 11 classifiers using the NLClassifier IBM Service
# 
# TODO: You must fill out all of the functions in this file following 
# 		the specifications exactly. DO NOT modify the headers of any
#		functions. Doing so will cause your program to fail the autotester.
#
#		You may use whatever libraries you like (as long as they are available
#		on CDF). You may find json, request, or pycurl helpful.
#

###IMPORTS###################################
#TODO: add necessary imports
import re
import pycurl
import subprocess


def field_filter(line):
    linelist = line.split('","')
    line = linelist[0] + '"' + linelist[5]
    return line

def html_tags(line):
    line = re.sub(r'<[^>]+>', '', line)
    return line

html_codes = {
    '&quot;':'"',
    '&amp;':'&',
    '&lt;':'<',
    '&gt;':'>',
}

def html_code(line):
    while (re.findall(r'&\w+;',line)):
        for key in html_codes:
            line = re.sub(key, html_codes[key], line)
    if re.search(r'&\w+;',line):
        print "Caution: find new html code!!"
        print re.search(r'&\w+;',line)
    return line

def urls(line):
    line = re.sub(r'(http|https|ftp):\/\/\S+( |$)','', line, flags=re.IGNORECASE)
    line = re.sub(r'www\.\w+\S*( |$)', '', line, flags=re.IGNORECASE)
    return line

def user_tags(line):
    while (re.findall(r'(@|#)(?P<reserve>\w+\S*( |$))',line)):
        reserve = re.search(r'(@|#)(?P<reserve>\w+\S*( |$))',line)
        reserve = reserve.group('reserve')
        line = re.sub(r'(@|#)(?P<reserve>\w+\S*( |$))', reserve, line, 1)
    length = len(line)
    # print length
    if line[length - 2] == '"':
        line = line[:length - 2] + line[length-1]
    #print line[length - 2]
    # print line
    return line

###HELPER FUNCTIONS##########################

def convert_training_csv_to_watson_csv_format(input_csv_name, output_csv_name, group_id = 97):
    # Converts an existing training csv file. The output file should
    # contain only the 11,000 lines of your group's specific training set.
    #
    # Inputs:
    #	input_csv - a string containing the name of the original csv file
    #		ex. "my_file.csv"
    #
    #	output_csv - a string containing the name of the output csv file
    #		ex. "my_output_file.csv"
    #
    # Returns:
    #	None

    #TODO: Fill in this function
    input_csv = open('./tweets/' + input_csv_name, 'r').readlines()
    if group_id != 0:
        zero = input_csv[group_id * 5500 : (group_id + 1) * 5500]
        four = input_csv[(800000 + group_id * 5500) : (800000 + (group_id + 1) * 5500)]
        zero.extend(four)
        lines = zero
    else:
        lines = input_csv
    output_csv = open('./tweets/' + output_csv_name, 'w')
    for line in lines:
        line_filter = field_filter(line)
        line_htag = html_tags(line_filter)
        line_hcode = html_code(line_htag)
        # print line_hcode
        line_urls = urls(line_hcode)
        # print line_urls
        line_processed = user_tags(line_urls)
        # print line_processed
        text = line_processed[3:len(line_processed) - 1]


        if len(text) > 1024:
            print("Error: the maximum total length of a text value is 1024 characters")
            break

        classtag = line_processed[1]

        re.sub("\t", "\\t", text)
        re.sub("\n", "\\n", text)

        pattern = "\,|\""
        if re.search(pattern, text):
            # text = text.split()
            text = text.split("\"")
            # for word in text:
            #     if word[0] == "\"":
            #         word = "\"" + word
            #     if word[-1] == "\"":
            #         word += "\""
            # text = " ".join(text)
            text = "\"\"".join(text)
            text = "\"" + text + "\""
        text = text.rstrip()
        line_processed = text + "," + classtag + "\n"
        line_processed = unicode(line_processed, errors='ignore')
        # line_processed = unicode(line_processed, "utf-8")
        # line_processed = line_processed.encode('utf-8')
        output_csv.write(line_processed)
    output_csv.close()
    return

def extract_subset_from_csv_file(input_csv_file, n_lines_to_extract, output_file_prefix='ibmTrain'):
    # Extracts n_lines_to_extract lines from a given csv file and writes them to
    # an outputfile named ibmTrain#.csv (where # is n_lines_to_extract).
    #
    # Inputs:
    #	input_csv - a string containing the name of the original csv file from which
    #		a subset of lines will be extracted
    #		ex. "my_file.csv"
    #
    #	n_lines_to_extract - the number of lines to extract from the csv_file, as an integer
    #		ex. 500
    #
    #	output_file_prefix - a prefix for the output csv file. If unspecified, output files
    #		are named 'ibmTrain#.csv', where # is the input parameter n_lines_to_extract.
    #		The csv must be in the "watson" 2-column format.
    #
    # Returns:
    #	None

    #TODO: Fill in this function
    if n_lines_to_extract < 5 or n_lines_to_extract > 5500:
        print("Error: the training data must have at least five records and no more than 15000 records")
        return
    input_csv = open('./tweets/' + input_csv_file, 'r').readlines()
    length_input = len(input_csv)
    lines1 = input_csv[:n_lines_to_extract]
    lines2 = input_csv[length_input / 2 : length_input / 2 + n_lines_to_extract]
    lines1.extend(lines2)
    lines = lines1
    output_csv = open('./tweets/' + output_file_prefix + str(n_lines_to_extract) + '.csv', 'w')
    output_csv.writelines(lines)
    return

def create_classifier(username, password, n, input_file_prefix='ibmTrain'):
    # Creates a classifier using the NLClassifier service specified with username and password.
    # Training_data for the classifier provided using an existing csv file named
    # ibmTrain#.csv, where # is the input parameter n.
    #
    # Inputs:
    # 	username - username for the NLClassifier to be used, as a string
    #
    # 	password - password for the NLClassifier to be used, as a string
    #
    #	n - identification number for the input_file, as an integer
    #		ex. 500
    #
    #	input_file_prefix - a prefix for the input csv file, as a string.
    #		If unspecified data will be collected from an existing csv file
    #		named 'ibmTrain#.csv', where # is the input parameter n.
    #		The csv must be in the "watson" 2-column format.
    #
    # Returns:
    # 	A dictionary containing the response code of the classifier call, will all the fields
    #	specified at
    #	http://www.ibm.com/smarterplanet/us/en/ibmwatson/developercloud/natural-language-classifier/api/v1/?curl#create_classifier
    #
    #
    # Error Handling:
    #	This function should throw an exception if the create classifier call fails for any reason
    #	or if the input csv file does not exist or cannot be read.
    #

    #TODO: Fill in this function
#
#
#     data = [
#         ('training_data', (
#         c.FORM_FILE, input_file_prefix + str(n) + '.csv')),
#         ('training_metadata', (
#         c.FORM_FILE, 'metadata.txt',
#         c.FORM_CONTENTTYPE, 'application/json'))
#
# ]
#     f = open('./classifier.csv', 'w')
#     f.write('classifier ' + str(n) + '\n')
    try:
        output = subprocess.check_output(['sh','./create_classifier.sh', username, password, str(n)])
        # f.write(output)
        return output
    except:
        print "Error"


if __name__ == "__main__":

    ### STEP 1: Convert csv file into two-field watson format
    input_csv_name = 'training.1600000.processed.noemoticon.csv'

    #DO NOT CHANGE THE NAME OF THIS FILE
    output_csv_name = 'training_11000_watson_style.csv'



    convert_training_csv_to_watson_csv_format(input_csv_name,output_csv_name)


    ### STEP 2: Save 11 subsets in the new format into ibmTrain#.csv files

    #TODO: extract all 11 subsets and write the 11 new ibmTrain#.csv files
    #
    # you should make use of the following function call:
    #
    n_lines_to_extract = [500, 2500, 5000]

    username = "bee79396-cad1-4f49-9e7e-42fde7a1349b"
    password = "0xnQMhbu1shc"


    f = open('./classifier_result.csv', 'w')

    for n in n_lines_to_extract:
        extract_subset_from_csv_file(output_csv_name, n)

    # f = open('./tweets/ibmTrain2500.csv', 'r').readlines()
    # f1 = open('./tweets/ibmTrain2500.csv', 'r').readlines()
    # print f[4249]
    # print f1[141]


    ### STEP 3: Create the classifiers using Watson

    #TODO: Create all 11 classifiers using the csv files of the subsets produced in
    # STEP 2
    #
    #
    # you should make use of the following function call
    # n = 500


        result = create_classifier(username, password, n, input_file_prefix='ibmTrain')
        f.write(result)
    f.close()

    # create_classifier(username, password, 502, input_file_prefix='ibmTrain')



